// Mendapatkan elemen yang dibutuhkan
const decrementButton = document.querySelector(".input-number-decrement");
const incrementButton = document.querySelector(".input-number-increment");
const inputNumber = document.querySelector(".input-number");
const priceElement = document.getElementById("price");
const priceValue = parseInt(priceElement.textContent.replace(/[^\d]/g, ""));

// console.log(priceValue);
var price = 0;
// Menambahkan event listener untuk tombol decrement
decrementButton.addEventListener("click", function () {
  decrementValue();
});

// Menambahkan event listener untuk tombol increment
incrementButton.addEventListener("click", function () {
  incrementValue();
});

// Fungsi untuk mengurangi nilai pada elemen input
function decrementValue() {
  let currentValue = parseInt(inputNumber.value);
  let min = parseInt(inputNumber.min);
  let newValue = currentValue - 1;
  if (newValue < min) {
    newValue = min;
  }
  inputNumber.value = newValue;
  updatePrice(newValue);
}

// Fungsi untuk menambah nilai pada elemen input
function incrementValue() {
  let currentValue = parseInt(inputNumber.value);
  let max = parseInt(inputNumber.max);
  let newValue = currentValue + 1;
  if (newValue > max) {
    newValue = max;
  }
  inputNumber.value = newValue;
  updatePrice(newValue);
}
// Fungsi untuk memformat nilai sebagai mata uang
function formatCurrency(value) {
  return "Rp." + numberWithCommas(value);
}
inputNumber.addEventListener("change", () => {
  price = inputNumber.value * priceValue;
  priceElement.textContent = formatCurrency(price);
});

// Fungsi untuk mengupdate nilai pada elemen harga
function updatePrice(value) {
  price = value * priceValue;
  priceElement.textContent = formatCurrency(price);
}

// Fungsi untuk menambahkan pemisah ribuan pada angka
function numberWithCommas(number) {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

// Memanggil fungsi updatePrice untuk menginisialisasi nilai pada elemen harga
updatePrice(inputNumber.value);

const popupContainer = document.querySelector(".background-popup");
const popup = document.querySelector(".popup");
const closeBtn = document.getElementById("closeBtn");
const buyBtn = document.getElementById("buyTicket");

closeBtn.addEventListener("click", () => {
  popupContainer.style.display = "none";
  popup.style.display = "none";

  // popup.classList.add("d-none");
  // popupContainer.classList.add("d-none");
  // closeBtn.classList.add("d-none");
});
buyBtn.addEventListener("click", () => {
  popupContainer.style.display = "block";
  popup.style.display = "block";
  window.scrollTo({
    top: 0,
    behavior: "smooth",
  });
  // closeBtn.classList.add("d-none");
});
